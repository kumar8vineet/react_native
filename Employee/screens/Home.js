
import { useLinkProps } from '@react-navigation/native';
import React,{useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,ActivityIndicator, Alert
} from 'react-native';
import {Card, FAB} from 'react-native-paper'
import {useSelector,useDispatch} from 'react-redux'


const Home=(props)=>{
  
  // const [data,setdata] = useState([])
  // const [loading,setLoading] = useState(true)  
  const {data,loading} = useSelector((state) =>{
      return state
  })
  const dispatch = useDispatch()
  
  const fetchData= ()=>{
    fetch("http://localhost:3000/")
    .then(res=>res.json())
    .then(results=>{
        // setdata(results)
        // setLoading(false)
        dispatch({type:"ADD_DATA", payload: results})
        dispatch({type:"SET_LOADING", payload: false})
        
    }).catch(err=>{
      Alert.alert("Something went wrong")
    })
  }
  useEffect(()=>{
    fetchData()
  },[])

  const renderList = ((item)=>{
    return(
      <Card style = {styles.mycard} key ={item._id}
       onPress={()=>props.navigation.navigate("Profile",{item})} >
      <View style = {styles.cardView}>
      
      <Image
      style={{width:60,height:60, borderRadius: 60/2}}
      source= {{uri: "https://images.unsplash.com/flagged/photo-1578848151039-b8916d7c1c34?ix_id=%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=521&q=80"}}
      />
      
      <View style={{marginLeft:10}}>
      <Text style={styles.text}>{item.name}</Text>
      <Text style={styles.text}>{item.position}</Text>
      </View>
      
      </View>
      
    </Card>
    )
  })
    return(
      <View style = {{flex:1}}>
        {
        loading?
          <ActivityIndicator size="large" color="0000ff"/>
          :
          <FlatList
          data = {data}
          renderItem={({item})=>{
            return renderList(item)
          }}
          keyExtractor={item=>`${item._id}`}
          onRefresh={()=>fetchData()}
          refreshing={loading}
          />
        }
        
           <FAB onPress = {()=> props.navigation.navigate("Create")}
              style={styles.fab}
              small={false}
              icon="plus"
              theme={{colors:{accent:"#006aff"}}}
              
            />
      </View>    
    )
}

const styles = StyleSheet.create({
  mycard: {
    margin:5,
    
  },
  cardView:{
    flexDirection:"row",
    padding:6
  },
  text:{
    fontSize: 15
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom:0
  },
})

export default Home