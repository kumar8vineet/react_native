import React, { useState } from 'react';
import {
  StyleSheet,
  View,Image,
  Text, Modal, Platform, Linking,Alert,TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Button, Card, Title} from 'react-native-paper'
import {Icon} from 'react-native-vector-icons'


const Profile = (props)=>{

    const{item} = props.route.params
        console.log(item._id);
    
    const deleteEmployee = ()=>{

        fetch("http://localhost:3000/delete",{
            method:"post",
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                id:item._id
            })
        })
        .then(res=>res.json())
        .then(deletedEmp =>{
            Alert.alert(`${deletedEmp.name} deleted`)
            props.navigation.navigate("Home")
        })
        .catch(err=>{
            Alert.alert("Something went wrong")
          })
    }

    const openDial = ()=>{
        if(Platform.OS === "android"){
            Linking.openURL ("tel:12345")
        }else{
            Linking.openURL("telprompt : 12345")
        }
    }

    return (
        <View style = {styles.root}>
            <LinearGradient
                colors = {["#0033ff","#6bc1ff"]}
                style={{height: "20%"}}
            />
        <View style = {{alignItems: "center", marginTop: -50}}>
            <Image
                style = {{width:140,height:140,borderRadius:70}}
                source={{uri:"https://images.unsplash.com/flagged/photo-1578848151039-b8916d7c1c34?ix_id=%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=521&q=80"}}
            />
            </View>
            <View style={{alignItems:"center"}}>
                <Title>{item.name}</Title>
                <Text style={{fontSize: 15}}>{item.position}</Text>
            </View>

            <Card style= {styles.mycard} onPress={()=>{
                Linking.openURL("mailto:vk@gmail.com")
            }}>
                <View style = {styles.cardContent}>
                    {/*<MaterialIcons name= "email" size ={32} color="#006aff"/>*/}
                    <Text style={styles.mytext}>{item.email}</Text>
                </View>
            </Card>
            
            <Card style= {styles.mycard} onPress={()=>{
                openDial
            }}>
                <View style = {styles.cardContent}>
                    {/*<Entypo name= "phone" size ={32} color="#006aff"/>*/}
                    <Text style={styles.mytext}>{item.phone}</Text>
                </View>
            </Card>
            
            <Card style= {styles.mycard}>
                <View style = {styles.cardContent}>
                    {/*<MaterialIcons name= "attach-money" size ={32} color="#006aff"/>*/}
                    <Text style={styles.mytext}>{item.salary}</Text>
                </View>
            </Card>
            
            <View style = {{flexDirection:"row", justifyContent:"space-around", padding:10}}>
            <TouchableOpacity
        onPress={() => {props.navigation.navigate("Create",
        {item})
        }}>
            <Text>Edit</Text>
      </TouchableOpacity>
               
                {/* <Button
                    icon="account-edit"
                    mode = "contained"
                    theme={theme}
                    onpress={()=> console.log("Pressed")}>
                        Edit
                </Button> */}

                <TouchableOpacity
        onPress={() => deleteEmployee()}>
            <Text>Fire Employee</Text>
      </TouchableOpacity>
             
              {/* <Button
                    icon="delete"
                    mode = "contained"
                    theme = {theme}
                    onpress={()=> 
                    console.log("pressed")}>
                        Fire Employee
                    </Button> */}

            </View>
        </View>
    )

}

const theme = {
    colors: {
        primary :"#006aff"
    }
}

const styles = StyleSheet.create({
    root:{
        flex:1
    },
    mycard:{
        margin:3
    },
    cardContent:{
        flexDirection:"row",
        padding: 8
    },
    mytext:{
        fontSize: 18,
        marginTop:3,
        marginLeft:5
    }
})


export default Profile