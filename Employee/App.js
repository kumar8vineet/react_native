/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import Home from './screens/Home'
import CreateEmployee from './screens/CreateEmployee'
import Profile from './screens/Profile'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import {reducer} from './reducers/reducer';


const store = createStore(reducer)

const Stack = createStackNavigator();

const App = () => {
  return (
    
      <>
        <View style = { styles.container}>
        
        <Stack.Navigator>
              <Stack.Screen name="Home" component={Home} />
              <Stack.Screen name="Create" component={CreateEmployee} />
              <Stack.Screen name="Profile" component={Profile} />
        </Stack.Navigator>

        </View>
      </>  
  );
};

export default ()=>{
  return(
    <Provider store = {store}>
    <NavigationContainer>
      <App/>
    </NavigationContainer>
    </Provider>
  )
}


const styles = StyleSheet.create({
  container: {
    backgroundColor: "#e0e0e0",
    flex: 1,
  },
});


